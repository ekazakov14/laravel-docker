const path = require('path');
const host = '0.0.0.0';
const port = '8081';
// variables
const isProduction = process.argv.indexOf('production') >= 0;
const sourcePath = path.join(__dirname, './resources');
const outPath = path.join(__dirname, './public');
// plugins
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WebpackCleanupPlugin = require('webpack-cleanup-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = {
    context: sourcePath,
    entry: {
        main: ['./app.tsx'],
    },
    optimization: {
        minimizer: isProduction ? [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})] : [],
    },
    output: {
        path: outPath,
        filename: 'js/app.js',
        publicPath: `http://localhost:${port}/`,
    },
    target: 'web',
    resolve: {
        extensions: ['.js', '.ts', '.jsx', '.tsx'],
        // Fix webpack's default behavior to not load packages with jsnext:main module
        // (jsnext:main directs not usually distributable es6 format, but es6 sources)
        mainFields: ['module', 'browser', 'main'],
        alias: {
            app: path.resolve(__dirname, 'resources/'),
            components: path.resolve(__dirname, 'resources/components/'),
            types: path.resolve(__dirname, 'resources/types/'),
            'react-dom': '@hot-loader/react-dom',
        },
    },
    module: {
        rules: [
            {
                test: [/\.tsx?$/, /\.ts?$/],
                use: [
                    {
                        loader: 'awesome-typescript-loader',
                        options: {
                            useCache: true,
                            useBabel: true,
                            babelOptions: {
                                babelrc: true,
                            },
                        },
                    },
                ],
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'postcss-loader', 'sass-loader'],
                }),
            },
            { test: [/\.png$/, /\.svg$/], use: 'url-loader?limit=10000' },
            { test: /\.jpg$/, use: 'file-loader' },
        ],
    },
    plugins: [
        new WebpackCleanupPlugin({
            exclude: ['.htaccess', 'favicon.ico', 'index.php', 'robots.txt'],
        }),
        new ExtractTextPlugin({
            filename: 'css/app.css',
            disable: !isProduction,
        }),
        new BundleAnalyzerPlugin({
            analyzerMode: (isProduction ? 'server' : 'disabled'),
        }),
    ],
    devServer: {
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        inline: true,
        host,
        hot: true,
        port,
    },
    devtool: 'cheap-module-eval-source-map',
    node: {
        fs: 'empty',
        net: 'empty',
    },
};
