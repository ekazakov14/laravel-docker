<?php

namespace Tests\Feature;

use \App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');


        $response->assertStatus(200);
    }

    public function testUserModel() {
        factory(User::class)->create();
        $count = User::all()->count();

        $this->assertEquals(1, $count);
    }
}
