<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
{{--        @if (app()->environment() === 'local')--}}
{{--            <link rel="stylesheet" href="http://localhost:8081/css/app.css">--}}
{{--        @else--}}
{{--            <link rel="stylesheet" href="https://localhost:8081/css/app.css">--}}
{{--        @endif--}}
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    </head>
    <body>
        <div id="app"></div>

        @if (app()->environment() === 'local')
            <script src="http://localhost:8081/js/app.js"></script>
        @else
            <script src="js/app.js"></script>
        @endif

    </body>
</html>
