import * as React from 'react';

interface Props {
  message: string;
}

const Page = ({ message = 'Hello world' }: Props) => <div>{message}</div>;

export default Page;
