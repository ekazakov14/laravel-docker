import './bootstrap';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { hot } from 'react-hot-loader';
import Page from './components/Page';

const App = hot(module)(() => (
  <Page message="2343123" />
));

ReactDOM.render(
  <App />,
  document.getElementById('app'),
);
